let slider = {
  init() {
    this.listImg = [
      {
        src: './images/huunghi.jpg',
        title: 'Huu Nghi Hotel'
      },
      {
        src: './images/dqd_2100(1)(2).jpg',
        title: 'Kukai Japanese restaurant'
      },
      {
        src: './images/nha-hang-chen.jpg',
        title: 'Chen restaurant'
      },
      {
        src: './images/catbaislandresort.jpg',
        title: 'Cat Ba island resort & spa'
      }
    ];
    this.$contentSliderImg = $('.js-content-slider-img img');
    this.$indicator = $('.js-indicator');
    this.$contentSliderTitle = $('.js-content-slider-title');
  },
  main() {
    this.init();
    this.firstSlider();
  },
  firstSlider() {
    this.$contentSliderImg.attr('src', this.listImg[0].src);
    this.$contentSliderTitle.text(this.listImg[0].title);
    this.$indicator.removeClass('is-active');
    this.$indicator.eq(0).addClass('is-active');
    this.runSlider(1);
  },
  runSlider(i) {
    setTimeout(() => {
      if(i > this.listImg.length - 1) {
        i = 0;
      }
      this.$contentSliderImg.fadeOut(400, () => {
        this.$contentSliderImg.attr('src', this.listImg[i].src);
        this.$contentSliderTitle.text(this.listImg[i].title);
        this.$indicator.removeClass('is-active');
        this.$indicator.eq(i).addClass('is-active');
        this.runSlider(++i);
      }).fadeIn(400);
    }, 3000);
  }
}

let modal = {
  init() {
    this.$openModal = $('.js-openModal');
    this.$modal = $('.js-modal');
    this.$closeModal = $('.js-closeModal');
    this.$modalLink = $('.js-modalLink');
  },
  main() {
    this.init();
    this.openModal();
    this.closeModal();
    this.modalLinkClick();
  },
  openModal() {
    this.$openModal.on('click', () => {
      this.$modal.addClass('is-active');
    });
  },
  closeModal() {
    this.$closeModal.on('click', () => {
      this.$modal.removeClass('is-active');
    });
  },
  modalLinkClick() {
    this.$modalLink.on('click', (e) => {
      this.$modalLink.removeClass('is-active');
      $(e.target).addClass('is-active');
    });
  }
}

$(() => {
  slider.main();
  modal.main();
});